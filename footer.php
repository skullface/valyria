<footer class="site-footer" role="contentinfo">
  <div class="copyright">
    <p>&copy; 2015 &mdash; ain’t no stoppin’ me &mdash; copywritten, so don’t copy me &mdash; じゃまた ね!</p>
  </div>
</footer>

<?php wp_footer(); ?>

<script>
var container = document.querySelector('.projects-grid');
var msnry = new Masonry( container, {
  // options
  itemSelector: '.projects-grid_project'
});
</script>

</body>
</html>
