<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="project-hero">
    <?php the_title( '<h1 class="project-title">', '</h1>' ); ?>

<?php
echo types_render_field( "hero-bg", array( "output" => "raw" ) )
?>

    <div class="project-description">
      <?php 
        echo types_render_field( "project-description", array( ) )
      ?>
    </div>
  </div>

	<?php the_content(); ?>

</section>