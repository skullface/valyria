<?php 
/*
Template Name: Homepage
*/

get_header(); ?>

<section class="all-projects">
  <ul class="projects-grid">
  <?php
  $my_query = new WP_Query('post_type=project&posts_per_page=-1');
  while ($my_query->have_posts()) : $my_query->the_post(); ?>
    <li class="projects-grid_project">
      <a href="<?php the_permalink() ?>" rel="bookmark" title="View <?php the_title_attribute(); ?>">
        <?php the_post_thumbnail(); ?>
        <span class="projects-grid_title"><?php the_title(); ?></span>
      </a>
    </li>
  <?php endwhile;  wp_reset_query(); ?>
  </ul>
</section>

<section class="site-about" id="about">
  <div class="about-bio">
    <h1 class="lede">Jessica Paoli builds sustainable brands and experiences with empathy in Cleveland, Ohio.</h1>

    <p>Hi, I’m Jessica, a multidisciplinary designer with an emphasis on web design and brand identity. I’m passionate about the work I do and the impact visual language and technology have on the world. I really love my cats, comic books, cookie dough, and the Oxford comma.</p>

    <p>If you have a cool project in need of design or just want to say hi, <a href="">email me</a> &mdash; I’m looking forward to hearing from you.</p>
  </div>
  <div class="about-portrait"></div>

  <div class="about-recognition">
    <h2>Recognition</h2>
    <ul class="recognition-list">
        <li>
            <strong>Bit Bash Chicago</strong>, featured artist
        </li>
        <li>
            <a href="http://jeremyrichie.net/the-back-and-forth/2014/5/6/the-back-forth-w-jessica-paoli"><strong>The Back and Forth</strong></a>, interview
        </li>
        <li>
            <strong>Ink Wars</strong> live illustration competition, participating artist
        </li>
        <li>
            <strong>Weapons of Mass Creation IV</strong>, featured designer
        </li>
        <li>
            <strong>Scholastic Art + Writing</strong>, regional Gold Key
        </li>
        <li>
            <strong>University of Rochester</strong>, Xerox Award for Innovation and Information Technology
        </li>
    </ul>
  </div>
</section>

<?php get_footer(); ?>